import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component';
import { UserManagementModule } from './user-management/user-management.module'

@NgModule({
  imports: [BrowserModule, UserManagementModule, AppRoutingModule],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
