import { Component } from '@angular/core';

import { UserService } from './user.service'

@Component({
  selector: 'user-management',
  templateUrl:"./user-management.component.html",
  providers:[UserService]
})
export class UserManagementComponent {
}
