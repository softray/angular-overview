import { Injectable } from '@angular/core'

import { Observable } from 'rxjs/Observable'
import { User } from './user'

@Injectable()
export class UserService {

  constructor() { }

  getAllUsers():Observable<User[]>{
    let users=[
      {id:0, firstName:"user 2", lastName:"user 1 last"},
      {id:1, firstName:"user 2", lastName:"user 2 last"},
      {id:2, firstName:"user 3", lastName:"user 3 last"}
    ];
     return Observable.of(users);
  }
}
