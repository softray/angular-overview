import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'

import { UserManagementComponent } from './user-management.component';
import { UserManagementRoutingModule } from './user-management-routing.module'

import { CardModule } from '../shared/card/index';
import { UsersComponent } from './users.component';
import { UserManageComponent } from './user-manage.component'


@NgModule({
    imports: [
      CommonModule,
      UserManagementRoutingModule,
      CardModule
    ],
    declarations: [UserManagementComponent, UsersComponent, UserManageComponent]
  })
  export class UserManagementModule { }