import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 's-card',
  templateUrl: './card.component.html',
  styles: []
})
export class CardComponent implements OnInit {

  @Input() caption:string;

  constructor() { }

  ngOnInit() {
  }

}
