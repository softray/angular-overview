import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { CardComponent } from './card.component';
import { CardBodyComponent } from './card-body.component'


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CardComponent, CardBodyComponent],
  exports:[CardComponent, CardBodyComponent]
})
export class CardModule { }
