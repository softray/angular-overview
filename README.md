## What is this repository for?

*   This repository is created for learning and sharing best coding practices in Angular5

## How do I get set up?

*   Install Node.js from [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
*   Install Angular CLI - from command window run 'npm install -g @angular/cli'
*   Clone this repository to your computer
*   In console window navigate to cloned repository and execute command 'npm install'
*   In console window navigate to cloned repository and run project 'ng serve'

## Covered lessons in this repository

1.  **Setup new angular project, angular solution arhitecture and feature modules** - more info on [https://angular.io/guide/architecture](https://angular.io/guide/architecture)
2.  **Custom shared components explained on card example** - more info on [https://material.angular.io/components/card/overview](https://material.angular.io/components/card/overview)

## Angular CLI

Angular now comes with a command line interface (CLI) to make it easier and faster to build Angular applications.

**The Angular CLI helps with:**

Bootstrapping a project

*   It creates the initial project structure with a root NgModule and a root component and bootstraps it using the platformBootstrapDynamic method.

Serving and live reloading

*   The CLI starts a local web-server so we can view our application in the browser via localhost:4000\. The CLI also watches for any changes to our files and automatically reloads the webpage if there are any.

Code generation

*   Using the CLI we can create components directives, services, pipes etc…​ all from the command line with all the necessary files, folders and boilerplate code included.

Testing

*   The generated code also comes with bootstrapped jasmine test spec files, we can use the CLI to compile and run all the tests with a single command. Whenever the CLI detects changes to any file it re-runs all the tests automatically in the background.

Packaging and releasing

*   The CLI doesn’t just stop with development, using it we can also package our application ready for release to a server.

**Installing Angular CLI:**

To install the CLI we use Node and npm.

npm install -g @angular/cli

**Start an application with ng-new**

ng-new angular-overview –minimal

## **Angular modules**

In Angular code is structured into packages called Angular Modules, or NgModules for short. Every app requires at least one module, the root module, that we call AppModule by convention.

To create a module using Angular CLI we can use command:

ng generate module [module name]

An NgModule is a class marked by the @NgModule decorator. @NgModule takes a metadata object that describes how to compile a component's template and how to create an injector at runtime. It identifies the module's own components, directives, and pipes, making some of them public, through the exports property, so that external components can use them. @NgModule can also add service providers to the application dependency injectors.

[https://angular.io/guide/ngmodules](https://angular.io/guide/ngmodules)

[https://angular.io/guide/architecture#components](https://angular.io/guide/architecture#components)

## **Components**

Components are a feature of Angular that let us create a new HTML _language_ and they are how we structure Angular applications. You can look at components as different parts of the actual page ie. the navigation menu bar, the sidebar, datepicker etc. In Angular we create new _custom_ tags with their own look and behaviour.

To create a new component using Angular CLI we can use following command:

ng generate component [component name]

Each component can have its own HTLM template code, CSS style code and background logic that enables functionality of the component itself.